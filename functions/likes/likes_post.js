const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.id_user) return util.bind(new Error('Id user cant be null!'))
    if (!body.id_post) return util.bind(new Error('Id post cant be null!'))

    var currentDate = new Date()
    currentDate = currentDate.now()

    const checkLikeExist = await mysql.query('select * from likes where id_user=?', [body.email])
    if (checkLikeExist.length) return util.bind(new Error('An account with this email already exists!'))

    const insert = await mysql.query('insert into likes values (?,?,?)', [body.id_user, body.id_post, currentDate])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
