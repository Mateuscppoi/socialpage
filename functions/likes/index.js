const get = require('./likes_get.js')
const post = require('./likes_post.js')
const put = require('./likes_put.js')
const remove = require('./likes_remove.js')

module.exports = {
  get,
  post,
  put,
  remove
}
