const get = require('./comments_get.js')
const post = require('./comments_post.js')
const put = require('./comments_put.js')
const remove = require('./comments_remove.js')

module.exports = {
  get,
  post,
  put,
  remove
}
