const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    if (event.pathParameters && event.pathParameters.id) {
      const comment = await mysql.query('select id, comment, id_user, id_post from comment where id=?', [event.pathParameters.id])
      return util.bind(comment.length ? comment[0] : {})
    }

    const comment = await mysql.query('select id, comment, id_user, id_post from comment')
    return util.bind(comment)
  } catch (error) {
    return util.bind(error)
  }
}
