const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.id) return util.bind(new Error('Enter comment code!'))
    if (!body.comment) return util.bind(new Error('Comment cant be null!'))

    await mysql.query('update comment set comment=? where id=? and id_user=?', [body.comment, body.id, body.id_user])
    return util.bind({})
  } catch (error) {
    return util.bind(error)
  }
}
