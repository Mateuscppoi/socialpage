const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.id_usuario) return util.bind(new Error('id user cant be null!'))
    if (!body.comment) return util.bind(new Error('Comment cant be null!'))
    if (!body.id_post) return util.bind(new Error('id post cant be null!'))

    var currentDate = new Date()
    currentDate = currentDate.now()
    const insert = await mysql.query('insert into commentvalues (?,?,?,?)', [body.comment, body.id_user, body.id_post, currentDate])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
