const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    if (event.pathParameters && event.pathParameters.id) {
      const log = await mysql.query('select id, name, email from log where id=?', [event.pathParameters.id])
      return util.bind(log.length ? log[0] : {})
    }

    const log = await mysql.query('select id, name, email from users')
    return util.bind(log)
  } catch (error) {
    return util.bind(error)
  }
}
