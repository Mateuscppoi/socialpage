const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.name) return util.bind(new Error('Enter your name!'))
    if (!body.password) return util.bind(new Error('Enter your password!'))
    if (!body.email) return util.bind(new Error('Enter your email!'))

    const checkUserExist = await mysql.query('select * from users where email=?', [body.email])
    if (checkUserExist.length) return util.bind(new Error('An account with this email already exists!'))
    var rand = function () {
      return Math.random().toString(36).substr(2) // remove `0.`
    }

    var token = function () {
      return rand() + rand() // to make it longer
    }

    var currentDate = new Date()
    currentDate = currentDate.now()

    const insert = await mysql.query('insert into users values (?,?,?,?,?,?)', [body.name, body.email, body.password, currentDate, currentDate, token()])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
