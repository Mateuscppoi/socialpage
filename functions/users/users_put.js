const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.id) return util.bind(new Error('Enter your code!'))
    if (!body.email) return util.bind(new Error('Email cant be null!'))
    if (!body.name) return util.bind(new Error('Name cant be null!'))
    if (!body.password) return util.bind(new Error('Password cant be null!'))
    const oldEmail = await mysql.query('select email from users where id=?', [body.id])
    if (oldEmail !== body.email) return util.bind(new Error('An account with this email already exists!'))

    await mysql.query('update users set name=?, email=?, password=? where id=?', [body.name, body.email, body.password, body.id])
    return util.bind({})
  } catch (error) {
    return util.bind(error)
  }
}
